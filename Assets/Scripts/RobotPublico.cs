﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotPublico : MonoBehaviour {

    private float waitTime;
    private Animator animController;
   
    // Use this for initialization
    void Start () {
        animController = GetComponent<Animator>();
        waitTime = Random.Range(5.0f, 20.0f);
        Invoke("InitCoRoutine", waitTime);
    }
	
	// Update is called once per frame
	void Update () {

    }

    void InitCoRoutine()
    {
        StartCoroutine("Animar");
    }

    IEnumerator Animar()
    {
        for (; ; )
        {
            Debug.Log("animar random");

            if (Random.Range(0f, 1.0f) < 0.5f)
               animController.SetTrigger("Animar");
            else
                animController.SetTrigger("Animar2");

            yield return new WaitForSeconds(waitTime);
        }
    }
}
