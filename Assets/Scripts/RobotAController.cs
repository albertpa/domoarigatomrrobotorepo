﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotAController : MonoBehaviour {

    public int currentLife = 100;
    public float speed = 2.0f;

    public bool isAttacking;
    public bool isDefending;
    private Animator animController;

    private AudioSource[] audios;
    private AudioSource audioHit1;
    private AudioSource audioHit2;
    private AudioSource audioHit3;
    private AudioSource audioHit4;

    private AudioSource audioSwipe;

    public ParticleSystem Tornillos;
    public ParticleSystem Tuercas;

    
    // Use this for initialization
    void Start () {

        animController = GetComponent<Animator>();
        audios = GetComponents<AudioSource>();
        audioHit1 = audios[0];
        audioHit2 = audios[1];
        audioHit3 = audios[2];
        audioHit4 = audios[3];
        audioSwipe = audios[4];

        isAttacking = false;
        isDefending = false;
}
	
	// Update is called once per frame
	void FixedUpdate () {

        isAttacking = animController.GetCurrentAnimatorStateInfo(0).IsName("Attack");
        isDefending = animController.GetCurrentAnimatorStateInfo(0).IsName("Defense");
        if (Input.GetKey("d"))
        {
            if (!isAttacking && !isDefending)
            {
                transform.position += Vector3.forward * Time.deltaTime * speed;
            }
        }

        if (Input.GetKey("a"))
        {
            if (!isAttacking && !isDefending)
            {
                transform.position += Vector3.back * Time.deltaTime * speed;
            }

        }

    }

    private void Update()
    {
        
        if (Input.GetKey("d"))
        {
            animController.SetFloat("Velocity", 1);
        }
      
       

        if (Input.GetKey("a"))
        {
            animController.SetFloat("Velocity", -1);
        }
        

        if (Input.GetKeyUp("d") || Input.GetKeyUp("a"))
        {
            animController.SetFloat("Velocity", 0);
        }
        
        

        if (Input.GetKeyDown("r"))
        {
           
            animController.SetBool("Defense", true);
        }
        if (Input.GetKeyUp("r"))
        {
            animController.SetBool("Defense", false);
        }

        if (Input.GetKeyDown("f"))
        {
            animController.SetTrigger("Attack");
        }
    }


    public void Attack()
    {

        RaycastHit hit;
        int layer_mask = LayerMask.GetMask("Default");

        Vector3 forward = transform.TransformDirection(Vector3.forward);

        Vector3 position = new Vector3(transform.position.x, transform.position.y+1, transform.position.z );


        Debug.DrawRay(position, forward * 1, Color.green);
        if (Physics.Raycast(position, forward, out hit, 100, layer_mask))
        {
            Debug.Log("HIT with" + hit.collider.gameObject.name+" in distance "+hit.distance);

            if (hit.distance < 2.4f && hit.collider.gameObject.tag == "RobotB")
            {
                Debug.Log("LE DA");

                RobotBController RB = hit.collider.gameObject.GetComponent<RobotBController>();

                if (RB.isDefending)
                    RB.currentLife -= 2;
                else
                    RB.currentLife -= 5;

                //se lanza uno de los 4 audios al azar
                int index = Random.Range(0, 4); //4 excluidos
                Debug.Log("audio:" + index);
                //BUG: no se porque, el primer audio 0 no suena, lo obvio

                AudioSource audioSeleccionado = audios[index];
                audioSeleccionado.Play();
                RB.Sangra();
            }
            else
            {
              
                audioSwipe.Play();
            }

        }

    }

    public void Sangra()
    {
        Tornillos.Play();
        Tuercas.Play();

    }
}
