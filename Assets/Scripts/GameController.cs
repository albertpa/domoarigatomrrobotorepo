﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {


    public Text lifeA;
    public Slider sliderA;

    public Text lifeB;
    public Slider sliderB;

    public Text win;

    public RobotAController RA;
    public RobotBController RB;
    // Use this for initialization
    void Start () {
        win.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(RA.currentLife >= 0){
            lifeA.text = RA.currentLife.ToString();
            sliderA.value = RA.currentLife;
        }
        else
        {
            lifeA.text = "0";
            sliderA.value = 0;
        }

        if (RB.currentLife >= 0)
        {
            lifeB.text = RB.currentLife.ToString();
            sliderB.value = RB.currentLife;
        }
        else
        {
            lifeB.text = "0";
            sliderB.value = 0;
        }

            if (RA.currentLife <= 0)
            win.text = "Robot B WINS!";

        if (RB.currentLife <= 0)
            win.text = "Robot A WINS!";

        if (RA.currentLife <= 0 || RB.currentLife <= 0)
        {
            win.enabled = true;
            Invoke("Reboot", 2f);
        }

    }

    void Reboot()
    {
        SceneManager.LoadScene("BattleScene");
    }
}
